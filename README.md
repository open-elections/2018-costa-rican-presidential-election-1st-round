# 2018 Costa Rican presidential election - 1st round

    start_date: 2018-02-04
    end_date: 2018-02-04
    source: http://resultados2018.tse.go.cr/resultadosdefinitivos/#/presidenciales
    wikipedia: https://en.wikipedia.org/wiki/2018_Costa_Rican_general_election